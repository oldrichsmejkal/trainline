﻿using System;
using System.Collections.Generic;

namespace AddressProcessing.CSV
{
    interface ICSVWriter:IDisposable
    {
        void Write(params string[] cols);
        void WriteAll(List<string[]> rows);
    }
}
