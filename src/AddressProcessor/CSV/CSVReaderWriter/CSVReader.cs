﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AddressProcessing.CSV
{
    public class CSVReader : ICSVReader
    {
        private StreamReader streamReader;
        private string filePath;
        private char separator;
        private int colsNum = -1;
        private int index = 0;

        public string[] Headers { get; private set; }
        public Dictionary<string, int> IndexByHeader { get; private set; }

        //custom headers have higher priority than headers in file
        public CSVReader(string filePath, char separator = '\t', bool fileContainsHeaders = false, string[] customHeaders = null)
        {
            this.filePath = filePath;
            this.separator = separator;

            if (!File.Exists(this.filePath))
            {
                throw new ArgumentException(string.Format("File doesnt exists: {0}", filePath));
            }
            streamReader = File.OpenText(filePath);
            InitHeaders(fileContainsHeaders, customHeaders);
        }

        private void InitHeaders(bool fileContainsHeaders, string[] customHeaders)
        {
            if (customHeaders != null)
            {
                InitHeadersByName(customHeaders);
            }

            if (fileContainsHeaders)
            {
                var line = streamReader.ReadLine();
                index++;
                if (line == null)
                {
                    throw new InvalidDataException("File should contain header, but is empty.");
                }
                if (IndexByHeader == null)
                {
                    InitHeadersByName(line.Split(separator));
                }  
            }
        }

        private void InitHeadersByName(string[] customHeaders)
        {
            colsNum = customHeaders.Length;
            Headers = customHeaders;
            IndexByHeader = new Dictionary<string, int>(colsNum);
            for (int i = 0; i < customHeaders.Length; i++)
            {
                IndexByHeader.Add(customHeaders[i], i);
            }
        }

        public string[] ReadLine()
        {
            var line = streamReader.ReadLine();
            index++;
            if (line == null)
                return null;

            var cols = line.Split(separator);
            if (colsNum == -1)
            {
                colsNum = cols.Length;
            }
            if (cols.Length != colsNum)
            {
                throw new InvalidDataException(string.Format("Line have incorrect number of collumns. Desired columns number: {0}, columns number: {1}, line: {2}", colsNum, cols.Length, line));
            }
            return cols;
        }

        public List<string[]> ReadLines()
        {
            List<string[]> result = new List<string[]>();
            var line = ReadLine();
            while (line != null)
            {
                result.Add(line);
                line = ReadLine();
            }
            return result;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (streamReader != null)
                    streamReader.Dispose();
            }
        }

    }
}
