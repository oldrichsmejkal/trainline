﻿using System;
using System.Collections.Generic;

namespace AddressProcessing.CSV
{
    interface ICSVReader:IDisposable
    {
        string[] ReadLine();
        List<string[]> ReadLines(); 
    }
}
