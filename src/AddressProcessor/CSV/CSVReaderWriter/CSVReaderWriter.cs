﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.
     *  
     *  
    */

    [Obsolete("This class exists only because backward compatibility and shouldn't be used in new code. Use CSVReader and CVSWriter instead.")]
    public class CSVReaderWriter:ICSVReaderWriter
    {
        private CSVReader csvReader;
        private CSVWriter csvWriter;
        private Mode mode;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public void Open(string fileName, Mode mode)
        {
            this.mode = mode;

            if (mode == Mode.Read)
            {
                csvReader = new CSVReader(fileName);
            }
            else if (mode == Mode.Write)
            {
                csvWriter = new CSVWriter(fileName);
            }
            else
            {
                throw new Exception("Unknown file mode for " + fileName);
            }
        }

        public void Write(params string[] columns)
        {
            if (mode != Mode.Write)
            {
                throw new Exception("Trying to write, but not opend in Write mode!");
            }
            csvWriter.Write(columns);
        }

        public bool Read(string column1, string column2)
        {
            return this.Read(out column1, out column2);
        }

        public bool Read(out string column1, out string column2)
        {
            if (mode != Mode.Read)
            {
                throw new Exception("Trying to read, but not opend in Read mode!");
            }
            var line = csvReader.ReadLine();
            
            if (line == null || line.Length == 0)
            {
                column1 = null;
                column2 = null;
                return false;
            }

            column1 = line[0];
            column2 = line[1];
            return true;
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (csvReader != null)
                    csvReader.Dispose();
                if(csvWriter != null)
                    csvWriter.Dispose();
            }
        }
    }
}
