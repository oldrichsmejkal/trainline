﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AddressProcessing.CSV
{
    public class CSVWriter:ICSVWriter
    {
        private StreamWriter streamWriter;
        private int colsNum = -1;
        private string separator;
        private int index = 0;

        public CSVWriter(string filePath, char separator = '\t', string[] headers = null)
        {
            this.separator = separator.ToString();
            FileInfo fileInfo = new FileInfo(filePath);
            streamWriter = fileInfo.CreateText();
            if (headers != null)
            {
                Write(headers);
            }
        }

        public void Write(params string[] cols)
        {
            if (cols == null)
            {
                throw new ArgumentException(string.Format("Parameter cols can't be null"));
            }
            if (index == 0)
            {
                colsNum = cols.Length;
            }
            if (cols.Length != colsNum)
            {
                throw new ArgumentException(string.Format("Cols have incorrect number of items. Desired items number: {0}, items number: {1}, items: {2}", colsNum, cols.Length, string.Join(separator, cols)));
            }
            index ++;
            streamWriter.WriteLine(string.Join(separator, cols));
        }

        public void WriteAll(List<string[]> rows)
        {
            foreach (var row in rows)
            {
                Write(row);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (streamWriter != null)
                    streamWriter.Dispose();
            }
        }
    }
}
