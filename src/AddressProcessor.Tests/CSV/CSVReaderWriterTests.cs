﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using AddressProcessing.CSV;
using NUnit.Framework;

namespace Csv.Tests
{
    [TestFixture]
    public class CSVReaderWriterTests
    {
        private const string testFile1 = @"test_data\test1.csv";
        private List<string[]> testDataWithoutHeader;
        private List<string[]> testDataWithoutHeaderBroken;
        private string[] testHeader;

        [SetUp]
        public void InitData()
        {
            testDataWithoutHeader = new List<string[]>();
            testDataWithoutHeader.Add(new[] { "aaa1", "bbb1" });
            testDataWithoutHeader.Add(new[] { "aaa2", "bbb2" });
            testDataWithoutHeader.Add(new[] { "aaa3", "bbb3" });
            testDataWithoutHeader.Add(new[] { "aaa4", "bbb4" });

            testDataWithoutHeaderBroken = new List<string[]>(testDataWithoutHeader);
            testDataWithoutHeaderBroken.Insert(2,new string[]{"onlyOneCol"});

            testHeader = new[] {"col1", "col2"};
        }

        [Test]
        public void WriteCSVAndReadBackWithoutHeader()
        {
            using (var csvWriter = new CSVWriter(testFile1))
            {
                csvWriter.WriteAll(testDataWithoutHeader);
            }

            List<string[]> readLines;
            using (var csvReader = new CSVReader(testFile1))
            {
                readLines = csvReader.ReadLines();
            }

            CSVDataEquals(testDataWithoutHeader, readLines);
        }

        [Test]
        public void WriteCSVAndReadBackWithHeader()
        {
            using (var csvWriter = new CSVWriter(testFile1,headers: testHeader))
            {
                csvWriter.WriteAll(testDataWithoutHeader);
            }

            List<string[]> readLines;
            string[] readHeader;
            using (var csvReader = new CSVReader(testFile1, fileContainsHeaders:true))
            {
                readHeader = csvReader.Headers;
                readLines = csvReader.ReadLines();
            }

            CSVDataEquals(testDataWithoutHeader, readLines);
            Assert.That(testHeader.SequenceEqual(readHeader), "Writed/Readed headers differs. Writed: {0}. Readed: {1}", string.Join(",", testHeader), string.Join(",", readHeader));
        }

        [Test]
        public void WriteCSVAndReadBackWithCustomHeader()
        {
            using (var csvWriter = new CSVWriter(testFile1, headers: testHeader))
            {
                csvWriter.WriteAll(testDataWithoutHeader);
            }

            List<string[]> readLines;
            string[] readHeader;
            var customHeader = new string[] {"h1","h2"};
            using (var csvReader = new CSVReader(testFile1, fileContainsHeaders: true,customHeaders:customHeader))
            {
                readHeader = csvReader.Headers;
                readLines = csvReader.ReadLines();
            }

            CSVDataEquals(testDataWithoutHeader, readLines);
            Assert.That(customHeader.SequenceEqual(readHeader), "Writed/Readed headers differs. Writed: {0}. Readed: {1}", string.Join(",", customHeader), string.Join(",", readHeader));
        }


        private void CSVDataEquals(List<string[]> writed, List<string[]> read)
        {
            Assert.That(writed.Count == read.Count,
                "Writed lines count: {0} does not equal to read lines count: {1}.", writed.Count, read.Count);
            for (int i = 0; i < writed.Count; i++)
            {
                Assert.That(writed[i].SequenceEqual(read[i]),"Writed/Readed lines differs. Writed: {0}. Readed: {1}", string.Join("," ,writed[i]), string.Join(",", read[i]));
            }
        }

        [Test]
        public void WriteCSVAndReadBackWithoutHeader_brokenData()
        {
            using (var csvWriter = new CSVWriter(testFile1))
            {
                Assert.Catch<ArgumentException>(() => { csvWriter.WriteAll(testDataWithoutHeaderBroken); }, "Invalid data wasn't correctly detected");
            }

        }

        [Test]
        public void WriteCSVAndReadBackWithHeader_accessByHeader()
        {
            using (var csvWriter = new CSVWriter(testFile1, headers: testHeader))
            {
                csvWriter.WriteAll(testDataWithoutHeader);
            }

            List<string[]> readLines;
            string[] readHeader;
            using (var csvReader = new CSVReader(testFile1, fileContainsHeaders: true))
            {
                int i = 0;
                var line = csvReader.ReadLine();
                while (line != null)
                {
                    var col1Read = line[csvReader.IndexByHeader["col1"]];
                    var col1Test = testDataWithoutHeader[i][0];
                    Assert.That(col1Read == col1Test,
                        "Data in col1 doesn't equals after write and read by column name. Test data: {0}, read data: {1}", col1Test, col1Read);

                    var col2Read = line[csvReader.IndexByHeader["col2"]];
                    var col2Test = testDataWithoutHeader[i][1];
                    Assert.That(col1Read == col1Test,
                        "Data in col2 doesn't equals after write and read by column name. Test data: {0}, read data: {1}", col2Test, col2Read);

                    line = csvReader.ReadLine();
                    i++;
                }
            }

        }

        [Test]
        public void CSVReaderWriterOriginalObsoleteClassTest()
        {
            using (var csvWriter = new CSVReaderWriter())
            {
                csvWriter.Open(testFile1,CSVReaderWriter.Mode.Write);
                foreach (var l in testDataWithoutHeader)
                {
                    csvWriter.Write(l);
                }
            }

            List<string[]> readLines;
            using (var csvReader = new CSVReaderWriter())
            {
                csvReader.Open(testFile1, CSVReaderWriter.Mode.Read);
                string col1 = null;
                string col2 = null;
                int i = 0;
                csvReader.Read(out col1, out col2);
                while (col1 != null)
                {
                    var actTestRow = testDataWithoutHeader[i];
                    Assert.That(col1 == actTestRow[0] && col2 == actTestRow[1], "Error in read writed values at index {i}", i);
                    i++;
                    csvReader.Read(out col1, out col2);
                }
            }

        }

        [TearDown]
        public void Cleanup()
        {
            if (File.Exists(testFile1))
                File.Delete(testFile1);
        }
    }
}
